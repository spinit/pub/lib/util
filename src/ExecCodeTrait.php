<?php

namespace Spinit\Util;

trait ExecCodeTrait
{
    /**
     * Esegue il codice passato come parametro memorizzandolo in un file temporaneo ed eseguendolo tramite
     * esecuzione della chiusura che ne isola lo scope.
     * Il file temporaneo viene cancellato dopo l'esecuzione e/o alla fine dello script
     * @param type $code
     * @param type $ENV
     * @param type $filerExtern
     * @return type
     */
    public function execCode($code, $ENV = array(), $filerName = '')
    {
        // pulizia codice
        $codeClean = ltrim(ltrim(trim($code), '<?php'), '<?');
        if (!$codeClean) {
            return null;
        }
        // generatore nome file. Può essere una funzione (che verrà richiamata) o 
        // una stringa (che verrà direttamente usata).
        // Nel caso non sia stato passato nulla allora viene generato un file temporaneo
        // @var $filerExtern string | callable
        $filer = function() use ($filerName) {
            if (getenv('SPINIT_UTIL_EXECCODE') == 'ERROR-NO-DELETE') {
                return sys_get_temp_dir().DIRECTORY_SEPARATOR.'OSY-CODE-'.$filerName;
            }
            $filename = tempnam(sys_get_temp_dir(), 'OSY-CODE-'.$filerName);
            unlink($filename);
            return $filename;
        };
        
        $data = new \ArrayObject();
        
        // determinazione nome del file
        $data['filename'] = $filer() . '.php';
        
        // salvataggio codice
        file_put_contents($data['filename'], "<?php\n{$codeClean}\n\n");
        
        $args = count($ENV) ? '$'.implode(', $', array_keys($ENV)) : '';
        
        // funzione che esegue il file che contiene il codice
        $callable = eval("return function({$args}) {return include ('{$data['filename']}');};");
        $data['status'] = '';
        // funzione che cancella il file eseguito
        $delete = function() use ($data) {
            if ($data['status'] != 'ok' and getenv('SPINIT_UTIL_EXECCODE') == 'ERROR-NO-DELETE') {
                return null;
            }
            return is_file($data['filename']) ? unlink($data['filename']) : null; 
        };
        
        // la funzione di cancellazione viene registrata anche in shutdown così da cancellare
        // il file temporaneo anche in caso di errore ... a meno che non è stato impostato
        // diversamente
        register_shutdown_function($delete);
        
        // esecuzione codice
        $result = call_user_func_array($callable, $ENV);
        
        $data['status'] = 'ok';
        // cancellazione file
        $delete();
        
        return $result;
    }
}
