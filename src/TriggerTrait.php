<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Util;

use Spinit\Util\Error;
use Webmozart\Assert\Assert;
use Spinit\Util;

/**
 * Trigger aggiunge alla classe la possibilità di poter gestire observer su eventi che vengono fatti partire su di esso.
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
trait TriggerTrait
{
    private $observer = array('before' => array(), 'exec' => array(), 'after' => array(), 'end' => array());
   
    protected static $staticObserver = array('before' => array(), 'exec' => array(), 'after' => array(), 'end' => array());
    /**
     * Segnaposti da implementare per intercettare la chiamata di un trigger generico
     * @param type $event
     * @param type $target
     */
    protected function onTrigger($event, $target) {}
    protected function afterTrigger($event, $target) {}
    
    /**
     * Costruttore dell'oggetto Evento
     * @param type $event
     * @param type $param
     * @return \ArrayObject
     */
    public function makeEvent($event, $param, $when = '')
    {
        $param = asArray($param);
        $eventStruct = array(
            'name'  => $event,
            'args'  => [],
            'target'=>$this,
            'when' => $when,
            'stopPropagation' => function(){
                throw new Error\StopTriggerException();
            },
            'getParam' => function($idx) use ($param) {
                return arrayGet($param, $idx, null, false);
            },
            'getParamList' => function() use ($param) {
                return $param;
            }
        );
        return new Util\Dictionary($eventStruct);
    }    
    /**
     * Invocazione di un evento
     * @param type $event
     * @param type $param
     */
    public function trigger($event, $param = array(), $onStop = null)
    {
        $EventStruct = $this->makeEvent($event, $param);
        $EventStruct['result'] = array();
        try {
            foreach(array_keys($this->observer) as $when) {
                $EventStruct['when'] = $when;
                $this->execTrigger($EventStruct, $this);
            }
        } catch (Error\StopTriggerException $ex) {
            if (is_callable($onStop)) {
                $onStop($ex);
            }
        }
        
        return $EventStruct['result'];
    }
    
    public function execTrigger($EventStruct, $target)
    {
        $this->onTrigger($EventStruct, $target);
        
        $when = $EventStruct['when'];
        
        $this->execTriggerObservers($EventStruct, $target, self::$staticObserver[$when]);
        $this->execTriggerObservers($EventStruct, $target, $this->observer[$when]);
        
        $this->afterTrigger($EventStruct, $target);
    }
    
    private function execTriggerObservers($EventStruct, $target, &$base) 
    {
        $events = asArray($EventStruct['name']);
        while(count($events)) {
            // evento non gestito?
            if (!isset($base[$events[0]])) {
                return;
            }
            $event = array_shift($events);
            $base = &$base[$event];
        }
        foreach($base as $call) {
            // se l'argomento individuato non è una funzione richiamabile
            $callVal = arrayGet($call, 0, '');
            if (!is_callable($callVal)) {
                $EventStruct['result'] = $callVal;
                continue;
            }
            $EventStruct['args'] = $events;
            $result = call_user_func($call[0], $EventStruct, $target, $call[1]);
            if ($result !== null) {
                $EventStruct['result'] = $result;
            }
        }
    }
    /**
     * Associazione di un evento ad una callback
     * @param type $when
     * @param type $event
     * @param type $callable
     * @param type $arguments
     */
    public function bind ($when, $event, $callable, $arguments = array())
    {
        Assert::keyExists($this->observer, $when, '('.implode(', ', array_keys($this->observer)).') : '.$when);
        if (!$this->observer[$when]) {
            $this->observer[$when] = [];
        }
        $base = &$this->observer[$when];
        foreach (Util\asArray($event) as $step) {
            if (!isset($base[$step])) {
                $base[$step] = [];
            }
            $base = &$base[$step];
        }
        $base[] = array($callable, $arguments);
    }
    
    public static function bindStatic ($when, $event, $callable, $arguments = array())
    {
        Assert::keyExists(self::$staticObserver, $when, '('.implode(', ', array_keys(self::$staticObserver)).') : '.$when);
        if (!self::$staticObserver[$when]) {
            self::$staticObserver[$when] = [];
        }
        $base = &self::$staticObserver[$when];
        foreach (Util\asArray($event) as $step) {
            if (!isset($base[$step])) {
                $base[$step] = [];
            }
            $base = &$base[$step];
        }
        $base[] = array($callable, $arguments);
    }
    /**
     * Scorciatoie
     */
    public function bindBefore($event, $callable, $arguments = array())
    {
        return $this->bind('before', $event, $callable, $arguments);
    }
    public function bindExec($event, $callable, $arguments = array())
    {
        return $this->bind('exec', $event, $callable, $arguments);
    }
    public function bindAfter($event, $callable, $arguments = array())
    {
        return $this->bind('after', $event, $callable, $arguments);
    }
    public function bindEnd($event, $callable, $arguments = array())
    {
        return $this->bind('end', $event, $callable, $arguments);
    }
}
