<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Util;

/**
 * Description of Session
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Session
{
    private static function dataSession()
    {
        if(!session_name()) {
            return '';
        }
        $session_name = preg_replace('/[^\da-z]/i', '', arrayGet($_COOKIE, session_name()));
        $session_file = nvl(session_save_path(), sys_get_temp_dir()).'/sess_'.$session_name;
        if (!is_file($session_file)) {
            return '';
        }
        return file_get_contents($session_file);
    }
    
    public static function get($name)
    {
        if (is_array($name)) {
            return self::mget($name);
        }
        $session_data = self::dataSession();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            if (!strstr(substr($session_data, $offset), "|")) break;
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            if ($varname == $name) {
                return $data;
            }
            $offset += strlen(serialize($data));
        }
        return null;
    }
    
    public static function mget($names)
    {
        if (!is_array($names)) {
            return self::get($names);
        }
        $session_data = self::dataSession();
        $list = [];
        $offset = 0;
        while ($offset < strlen($session_data) and count($names)) {
            if (!strstr(substr($session_data, $offset), "|")) break;
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            if (in_array($varname, $names)) {
                $list[$varname] = $data;
                unset($names[$varname]);
            }
            $offset += strlen(serialize($data));
        }
        return $list;
    }
    
    public static function set($name, $value = null)
    {
        // questa riga evita che la chiamata session_start aggiunga in automatico gli header che disabilitano la cache
        @session_cache_limiter('private_no_expire:');
        @session_start();
         if (is_array($name)) {
            $list = [];
            foreach($name as $n => $v) {
                $_SESSION[$n] = $v;
            }
            $value = $name;
        } else {
            $_SESSION[$name] = $value;
        }
        @session_write_close();
        return $value;
    }
}