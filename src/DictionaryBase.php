<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Util;

/**
 * Description of DictionaryBase
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class DictionaryBase implements \ArrayAccess, \Iterator, \Countable, \JsonSerializable
{
    private $repo = [];
    /**
     * Inizializzazione
     * @param array $init
     */
    public function __construct($init = array())
    {
        if (!$init) {
            $init = array();
        }
        if (is_string($init)) {
            throw new \Exception('Bad Init Data');
        }
        if ($init instanceof self) {
            $init = $init->asArray();
        }
        $this->repo = $init;
    }
    
    /**
     * Estrazione elemento in testa
     * @return type
     */
    public function shift()
    {
        return array_shift($this->repo);
    }
    
    /**
     * Inserimento elemento in testa
     * @param type $data
     * @return $this
     */
    public function unshift($data)
    {
        array_unshift($this->repo, $data);
        return $this;
    }
    
    /**
     * Serializzazione json
     * @return type
     */
    public function jsonSerialize() 
    {
        return $this->asArray();
    }
    
    /**
     * Recupero dati wrappati
     * @return type
     */
    public function asArray()
    {
        return $this->repo;
    }
    
    /**
     * L'invocazione richiede la chiave
     * @param type $key
     * @return type
     */
    public function __invoke($key, $args = [])
    {
        $val = $this->get($key);
        if (is_callable($val)) {
            return call_user_func_array($val, $args);
        }
        return $val;
    }
    
    /**
     * invoca la callable associata
     * @param type $method
     * @param type $args
     */
    public function __call($method, $args)
    {
        // se la chiave non è difinita allora viene invoata una eccezione
        $val = $this->get($method, function() use ($method) {
            throw new \Exception('call : '.$method);
        });
        // se alla chiave è associata una chiusura ... viene invocata
        if (is_callable($this->get($method))) {
            return call_user_func_array($this->get($method), $args);
        }
        // altrimenti viene ritornato il valore
        return $val;
    }

    private function addValue($key, $value, $append = false, $first = false)
    {
        $ksearch = explode('.', $key);
        $klast   = count($ksearch)-1;
        $target  =& $this->repo;
        foreach ($ksearch as $i => $k) {
            if ($klast == $i) {
                // è una lista di valori o è unico?
                if (!$append) {
                    if (!is_array($target)) {
                        $target = array();
                    }
                    $target[$k] = $value;
                } elseif (is_array(@$target[$k])) {
                    // deve essere messo come primo valore o in coda?
                    if ($first) {
                        array_unshift($target[$k], $value);
                    } else {
                        $target[$k][] = $value;
                    }
                } else {
                    $target[$k] = array($value);
                }
            } elseif (is_array($target) && array_key_exists($k, $target)) {
                $target = &$target[$k];
            } elseif (count($ksearch) != ($i+1)) {
                $target[$k] = array();
                $target =& $target[$k];
            }
        }
        
        return $this;
    }
    
    public function append()
    {
        $args = func_get_args();
        $value = array_pop($args);
        $key = call_user_func_array(array($this, 'buildKey'), $args);
        $this->addValue($key, $value, true);
        return $this;
    }    
    
    public function prepend()
    {
        $args = func_get_args();
        $value = array_pop($args);
        $key = call_user_func_array(array($this, 'buildKey'), $args);
        $this->addValue($key, $value, true, true);
        return $this;
    }
    
    /**
     * Generazione chiave
     * @return type
     */
    public function buildKey()
    {
        return implode('.', func_get_args());
    }
    
    /**
     * Recupero dati dalla struttura
     * @param type $key
     * @param type $default
     * @return type
     */
    public function &get($key, $default = null, $notEmpty = false)
    {
        $sep = '.';
        if (is_array($key)) {
            $sep = array_shift($key);
            $key = array_shift($key);
        }
        $ksearch = explode($sep, $key);
        $target =& $this->repo;
        foreach ($ksearch as $k) {
            if (!array_key_exists($k, $target ?: [])) {
                if (is_callable($default)) {
                    return call_user_func($default, $k, $key);
                }
                if ($notEmpty) {
                    throw new Error\NotFoundException('key not found : '.$key);
                }
                return $default;
            }
            $target = &$target[$k];
        }
        if (!$target and $target !== '0' and $notEmpty) {
            return $default;
        }
        return $target;
    }
    
    /**
     * Impostazione dei dati nella struttura
     * @return $this
     */
    public function set()
    {
        $args = func_get_args();
        $value = array_pop($args);
        $key = call_user_func_array(array($this, 'buildKey'), $args);
        $this->addValue($key, $value);
        return $this;
    }
    
    /**
     * Aggiunta di un valore ... dove ne è presente un altro
     *
     * @param type $key
     * @param type $value
     * @return $this
     */
    public function merge($key, $value)
    {
        $prop = $this->get($key, null);
        if (!is_array($prop)) {
            // il vecchio valore non è un array
            if (!is_array($value)) {
                // se il valore non è un array ... allora verrà effettuata una sostituzione
                $this->append($key, $value);
            } else {
                // altrimenti si cerca di preservare il vecchio e il nuovo
                if ($prop != null) {
                    $this->set($key, array_merge(array($prop), $value));
                } else {
                    $this->set($key, $value);
                }
            }
        } else {
            // la chiave sulla quale si sta effettuando una merge + un array.
            // quindi occorre unire il vecchio con il nuovo
            if (!is_array($value)) {
                $value = array($value);
            }
            $prop = array_merge($prop, $value);
            $this->set($key, $prop);
        }
        return $this;
    }
    
    /**
     * Verifica della presenta di un valore su una chiave
     * @param type $key
     * @return type
     */
    public function keyExists($key)
    {
        $ksearch = explode('.', $key);
        $target = $this->repo;
        $nnode = count($ksearch);
        foreach ($ksearch as $k) {
            if (is_array($target)) {
                if (array_key_exists($k, $target)) {
                    $target = $target[$k];
                } else {
                    break;
                }
                $nnode--;
            }
        }
        return $nnode ? false : true;
    }
    
    public function offsetSet( $offset,  $value) 
    {
        if (is_null($offset)) {
            $this->repo[] = $value;
        } else {
            $this->set($offset, $value);
        }
    }

    public function offsetExists( $offset) 
    {
        return isset($this->repo[$offset]);
    }

    public function offsetUnset( $offset) 
    {
        unset($this->repo[$offset]);
    }

    public function &offsetGet( $offset) 
    {
        return $this->get($offset);
    }
    
    public function rewind() 
    {
        reset($this->repo);
    }

    public function current() 
    {
        return current($this->repo);
    }

    public function key() 
    {
        return key($this->repo);
    }

    public function next() 
    {
        next($this->repo);
    }

    public function valid() 
    {
        return $this->current() !== false;
    }

    public function count() 
    {
        return count($this->repo);
    }
}
