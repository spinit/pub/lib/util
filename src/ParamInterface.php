<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Util;

/**
 * Description of ParamInterface
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface ParamInterface
{
    public function getParam($name);
}
