<?php

namespace Spinit\Util;

class FactoryMap {
    
    static private $maps;
    
    public static function get() {
        $args = func_get_args();
        $arch = array_shift($args);
        $nameClass = getColonsPath(array_shift($args));
        
        return getInstanceArray(arrayGet(self::$maps, [$arch, $nameClass], $nameClass), $args);
    }
    
    /**
     * 
     * @param string $arch : Architettura 
     * @param string $name : classpath da sostituire
     * @param string $cpath : classpath sostituito
     */
    public static function set($arch, $name, $cpath) {
        self::$maps[$arch][getColonsPath($name)] = getColonsPath($cpath);
    }
    
}
