<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Util\UnitTest;

use PHPUnit\Framework\TestCase;
use Spinit\Util\Configuration;

/**
 * Description of DictionaryTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ConfigurationTest extends TestCase
{
    
    public function testDirectory()
    {
        $config = new Configuration(__DIR__);
        $this->assertEquals(__DIR__, $config->getBase());
        $this->assertEquals(dirname($config->getBase()).DIRECTORY_SEPARATOR.'conf', $config->getRoot());
    }
    
    public function testConfigJsonFound()
    {
        $config = new Configuration(__DIR__, '../conf/json');
        $this->assertEquals('ok', $config->get('keyFound'));
        $this->assertFalse($config->keyExists('ketUnknown'));
    }
    public function testConfigNotFound()
    {
        $config = new Configuration(__DIR__);
        $this->assertNotEquals('ok', $config->get('keyFound'));
    }
    public function testConfigXml()
    {
        $config = new Configuration(__DIR__, '../conf/xml');
        $this->assertEquals('ok', $config->get('keyXmlFound'));
        $this->assertFalse($config->keyExists('ketUnknown'));
    }
    
    /**
     * @expectedException \Exception
     */
    public function testBadConfig()
    {
        $this->expectException(\Exception::class);
        new Configuration('ambarabaciccicocco');
    }
}
