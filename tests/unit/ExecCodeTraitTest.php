<?php
namespace Spinit\Util\UnitTest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Util\ExecCodeTrait;
use PHPUnit\Framework\TestCase;


/**
 * Description of ExecCodeTraitTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ExecCodeTraitTest extends TestCase
{
    use ExecCodeTrait;
    
    public function testExecCode()
    {
        $response = $this->execCode('return "hello {$name}";', array('name'=>'world'));
        $this->assertEquals('hello world', $response);
    }
    public function testExecCodeWithFile()
    {
        $response = $this->execCode('return "hello {$name}";', array('name'=>'world'), "/tmp/my-code");
        $this->assertFalse(is_file('/tmp/my-code.php'));
        $this->assertEquals('hello world', $response);
    }
}
