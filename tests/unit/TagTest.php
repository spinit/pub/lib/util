<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Util\UnitTest;

use Spinit\Util\Tag;
use PHPUnit\Framework\TestCase;

/**
 * Description of TagTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class TagTest extends TestCase
{
    function testTag()
    {
        $a = new Tag();
        $this->assertEquals('', $a->getTag());
        $this->assertEquals('""', json_encode($a));
        $this->assertTrue($a->isEmpty());
        $a->add('test');
        $this->assertEquals('"test"', json_encode($a));
        $input = $a->add(new Tag('input', 'idtag'));
        $this->assertEquals('"test<input id=\"idtag\"\/>"', json_encode($a->get(false)));
        $input->class = 'bold';
        $this->assertEquals('"test\n  <input id=\"idtag\" class=\"bold\"\/>"', json_encode($a));
        $this->assertEquals('"test"', json_encode($a->child(0)));
        $this->assertEquals('"<input id=\"idtag\" class=\"bold\"\/>"', json_encode($a->child(1)));
        $this->assertEquals(2, count($a->child()));
        $this->assertEquals(false, $a->child(-1));
    }
    public function testAdd()
    {
        $tag = new Tag('test');
        $tag->add(array(new Tag('una'), new Tag('prova')));
        $this->assertEquals('<una></una>', trim($tag->child(0)));
        $this->assertEquals(2, count($tag->child()));
        $tag->add(new Tag('br'), 'pre');
        $this->assertEquals('<br/>', trim($tag->child(0)));
        $this->assertEquals(3, count($tag->child()));
    }
    
    public function testSetParent()
    {
        $primo = new Tag('primo');
        $secondo = new Tag('secondo');
        $child = new Tag('br', 'ok');
        $primo->add($child);
        $this->assertEquals('<primo><br id="ok"/></primo>', $primo->get(false));
        $this->assertEquals('<secondo></secondo>', $secondo->get(false));
        $this->assertFalse($primo->isEmpty());
        $secondo->add($child);
        $this->assertTrue($primo->isEmpty());
        $this->assertEquals('<primo></primo>', $primo->get(false));
        $this->assertEquals('<secondo><br id="ok"/></secondo>', $secondo->get(false));
        $this->assertFalse($secondo->isEmpty());
        $child->remove();
        $this->assertTrue($secondo->isEmpty());
        $this->assertEquals('<secondo></secondo>', $secondo->get(false));
        $secondo->add('ok');
        $this->assertFalse($secondo->isEmpty());
        $this->assertEquals('<secondo>ok</secondo>', $secondo->get(false));
    }
    
    public function testBuild()
    {
        $div = Tag::create('div');
        $div->prova = array(1,2);
        $div->add('test');
        $this->assertEquals('<div prova="[1,2]">test</div>', $div->get());
        $div->set();
        $this->assertEquals('<div prova="[1,2]"></div>', $div->get());
        $div->set('uno', Tag::create('br'));
        $this->assertEquals('<div prova="[1,2]">uno<br/></div>', $div->get(false));
        $div->remove();
        $this->assertEquals('', $div->get(false));
    }
    
    /**
     */
    public function testGetElementById()
    {
        $this->expectException(\Exception::class);        
        $div = Tag::create('div','okid');
        $this->assertEquals($div, Tag::getElementById('okid'));
        Tag::getElementById('___not found_element', function(){throw new \Exception('OK TEST');});
    }
    
    public function testAtt()
    {
        $div = Tag::create('div','ok2');
        $div->att(array('uno'=>'1', 'due'=>'2'));
        $this->assertEquals('<div id="ok2" uno="1" due="2"></div>', $div->get(false));
        $div->att('uno','1','+');
        $this->assertEquals('<div id="ok2" uno="1+1" due="2"></div>', $div->get(false));
        $this->assertEquals($div, Tag::getElementById('ok2'));
        $div->id = 'newid';
        $this->assertEquals($div, Tag::getElementById('newid'));
        $this->assertEquals(false, Tag::getElementById('ok2'));
    }
    
    public function testAdd2()
    {
        $div = Tag::create('div','ok2');
        $div->add(Tag::create('div','ok3'));
        $this->assertEquals('<div id="ok2"><div id="ok3"></div></div>', $div->get(false));
    }
}
